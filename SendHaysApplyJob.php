<?php

namespace App\Jobs\Scrappy\Hays;

use App\Jobs\ScriptedBrowserJob;
use App\Jobs\SendApplicationEmailJob;
use App\Models\HopperJobArchive;
use App\Models\Seeker\Application;
use App\Services\MailService;
use App\Services\QueueManager;
use App\Services\Scrappy\Hays\HaysApplyService;
use Carbon\Carbon;
use Exception;
use Facebook\WebDriver\Remote\LocalFileDetector;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class SendHaysApplyJob extends ScriptedBrowserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    public $applyId;

    /**
     * Create a new job instance.
     *
     * @param $applyId
     *
     * @return void
     */
    public function __construct($applyId)
    {
        $this->applyId = $applyId;

    }

    public function validateCaptchaInput($recpatchaToken)
    {
        $js_func = "
            window.bitir = function() {
                    var taskSolution='" . $recpatchaToken . "';
                    var injectedCode = \"(\" + function(taskSolution) {
                                    var recaptchaCallbackAlreadyFired = false;
                                    
                                    var recursiveCallbackSearch = function(object, solution, currentDepth, maxDepth) {
                                        if (recaptchaCallbackAlreadyFired) {
                                            return
                                        }
                                        var passedProperties = 0;
                                        for (var i in object) {
                                            passedProperties++;
                                            if (passedProperties > 15) {
                                                break
                                            }
                                            try {
                                                if (typeof object[i] == \"object\" && currentDepth <= maxDepth) {
                                                    recursiveCallbackSearch(object[i], solution, currentDepth + 1, maxDepth)
                                                } else if (i == \"callback\") {
                                                    if (typeof object[i] == \"function\") {
                                                        recaptchaCallbackAlreadyFired = true;
                                                        object[i](solution)
                                                    } else if (typeof object[i] == \"string\" && typeof window[object[i]] == \"function\") {
                                                        recaptchaCallbackAlreadyFired = true;
                                                        window[object[i]](solution)
                                                    }
                                                    return
                                                }
                                            } catch (e) {}
                                        }
                                    };
                                    
                                    if (!recaptchaCallbackAlreadyFired) {
                                        if (typeof ___grecaptcha_cfg != \"undefined\" && typeof ___grecaptcha_cfg.clients != \"undefined\") {
                                            var oneVisibleRecaptchaClientKey = null;
                                            visible_recaptcha_element_search_loop: for (var i in ___grecaptcha_cfg.clients) {
                                                for (var j in ___grecaptcha_cfg.clients[i]) {
                                                    if (___grecaptcha_cfg.clients[i][j] && typeof ___grecaptcha_cfg.clients[i][j].nodeName == \"string\" && typeof ___grecaptcha_cfg.clients[i][j].innerHTML == \"string\" && typeof ___grecaptcha_cfg.clients[i][j].innerHTML.indexOf(\"iframe\") != -1) {
                                                        if (___grecaptcha_cfg.clients[i][j].offsetHeight != 0 || ___grecaptcha_cfg.clients[i][j].childNodes.length && ___grecaptcha_cfg.clients[i][j].childNodes[0].offsetHeight != 0 || ___grecaptcha_cfg.clients[i][j].dataset.size == \"invisible\") {
                                                            if (oneVisibleRecaptchaClientKey === null) {
                                                                oneVisibleRecaptchaClientKey = i;
                                                                break
                                                            } else {
                                                                oneVisibleRecaptchaClientKey = null;
                                                                break visible_recaptcha_element_search_loop
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (oneVisibleRecaptchaClientKey !== null) {
                                                recursiveCallbackSearch(___grecaptcha_cfg.clients[oneVisibleRecaptchaClientKey], taskSolution, 1, 2)
                                            }
                                        }
                                    }
                                } + ')(\"' + taskSolution + '\");';
                                var script = document.createElement(\"script\");
                                script.textContent = injectedCode;
                                (document.head || document.documentElement).appendChild(script);
                                script.remove();
                }
        ";

        return $js_func;
    }


    /**
     * Execute the job. This is where the selenium instructions will take place.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function handle()
    {
        $haysApplyService = new HaysApplyService(4);

        // Initialise global variables
        $error_messages = [];
        $hardFail  = false;
        $noResumeFlag   = false;
        $expiredJob = false;
        $resumePath     = null;

        try {
            /** @var  Application $application */
            $application = Application::query()
                ->where('id', $this->applyId)
                ->with(['seeker','resume','coverLetter'])
                ->first();

            if (empty($application->seeker)) {
                $hardFail = true;
                throw new Exception('Application missing seeker data');
            } elseif (empty($application->resume)) {
                $noResumeFlag = true;
                throw new Exception('Application missing resume');
            } elseif (empty($application->job_uuid)) {
                $expiredJob = true;
                throw new Exception('JOB EXPIRED');
            }

            /** @var  HopperJobArchive $job */
            $job = HopperJobArchive::query()->where('uuid', $application->job_uuid)->firstOrFail();

            // Download resume to server
            try {
                $resumePath = $haysApplyService->getResume($job, $application);
            } catch (Exception $e) {
                $hardFail = true;
                throw new Exception('Resume not of .pdf, .doc or .docx format.');
            }

            // Check for resume max file size before engaging browserstack
            $resumeSize   = filesize($resumePath);
            $resumeSizeKB = ($resumeSize / 1024);

            if ($resumeSizeKB > 499) {
                $hardFail = true;
                throw new Exception('Failed To Apply - Resume file size greater than 500KB.');
            }
            $g_recaptcha_response     = $haysApplyService->solveCaptcha();
            $captcha_solved_timestamp = Carbon::now();

            logger()->info('HaysCustomApply - Captcha token generated - Apply ID - ' . $this->applyId . ' ' . $captcha_solved_timestamp);

            // Instantiate webdriver (API call to browserstack remote server)
            $driver = establish_browserstack_session("Hays", $this->applyId, true);
//            $driver = establish_local_session("Hays", $this->applyId, true);

            logger()->info('HaysCustomApply - Browser instantiated - Apply ID - ' . $this->applyId . ' ' . Carbon::now());

            try {
                // Get job landing page
                $driver->get($job->apply_url);
                sleep(4);
            } catch (Exception $e) {
                expire_jobs_from_smartfeed($application->job_uuid);
                $expiredJob = true;
                throw new Exception('JOB EXPIRED');
            }

            // Check for expired job, if expired then terminate driver and log error
            if (count($driver->findElements(WebDriverBy::id('gtm_jobSummary_apply_button'))) === 0) {
                expire_jobs_from_smartfeed($application->job_uuid);
                $expiredJob = true;
                throw new Exception('JOB EXPIRED');
            }
            sleep(5);

            try {
                $driver->switchTo()->frame($driver->findElement(WebDriverBy::xpath(".//iframe[@title='TrustArc Cookie Consent Manager']")));
                sleep(5); // sleep enough time to let iframe load
                $driver->findElement(WebDriverBy::xpath("//a[@class='call']"))->click();
                $driver->switchTo()->frame(0);
            } catch (Exception $e) {
                logger()->debug($e->getMessage());
            }
            sleep(2);

            $driver->findElement(WebDriverBy::id('gtm_jobSummary_apply_button'))->click();
            sleep(2);

            $driver->findElement(WebDriverBy::id('gtm_showSendCV_btn'))->click();
            sleep(2);

            wait_for_element_by_xpath($driver, 10, 500, '//input[@formcontrolname="emailAddress"]');

            // Check if seeker details exist in apply record, if not fetch details from seeker table
            $driver->findElement(WebDriverBy::xpath('//input[@formcontrolname="firstname"]'))
                ->sendKeys((empty($application->first_name) ? $application->seeker->first_name : $application->first_name));

            $driver->findElement(WebDriverBy::xpath('//input[@formcontrolname="lastname"]'))
                ->sendKeys((empty($application->last_name) ? $application->seeker->last_name : $application->last_name));

            $driver->findElement(WebDriverBy::xpath('//input[@formcontrolname="emailAddress"]'))->sendKeys($application->seeker->email);

            $driver->findElement(WebDriverBy::id("uploadInput"))->setFileDetector(new LocalFileDetector())
                ->sendKeys($resumePath);

            sleep(1);

            $driver->findElement(WebDriverBy::name('Policy'))->click();

            if ($captcha_solved_timestamp->diffInSeconds(Carbon::now()) >= 105) {
                $g_recaptcha_response = $haysApplyService->solveCaptcha();
                logger()->info('HaysCustomApply - Captcha token regenerated - Apply ID - ' . $this->applyId . ' ' . Carbon::now());
            }

            // Sleep to let JS render required fields
            $driver->executeScript('document.getElementById("g-recaptcha-response").innerHTML="' . $g_recaptcha_response . '";');
            $driver->executeScript($this->validateCaptchaInput($g_recaptcha_response));
            $driver->executeScript("window.bitir()");

            logger()->info('HaysCustomApply - About to submit application - Apply ID - ' . $this->applyId . ' ' . Carbon::now());

            sleep(1);

            $driver->findElement(WebDriverBy::id('gtm_job_apply_btn'))->click();

            sleep(4);

            $errorFields = $driver->findElements(WebDriverBy::className('error-msg'));
            foreach ($errorFields as $error) {
                if (strpos($error->getAttribute('class'), 'hidden') === false) {
                    array_push($error_messages, $error->getText());
                }
            }

            // Check for any errors (required fields)
            if (count($error_messages) > 0) {
                throw new Exception('Failed To Apply - ' . implode(", ", $error_messages));
            }

            // Check for apply landing page, if not then flag apply as unsuccessful
            $successfulApply = $driver->findElements(WebDriverBy::xpath('//app-jp-apply-confirmation'));
            if (count($successfulApply) === 0) {
                throw new Exception('Failed to apply - did not land on thank you page');
            }

            $application->update([
                'delivery_status'  => 'successful',
                'delivery_message' => null
            ]);

            // Close browser
            $driver->quit();

            // Delete resume file from local storage
            $haysApplyService->deleteDocuments($resumePath);
        } catch (Exception $e) {
            logger()->info('HaysCustomApply - Apply failed - Apply ID - ' . $this->applyId . " error message: " . $e->getMessage());
            // Terminate browserstack session if still open
            if (!empty($driver)) {
                $driver->quit();
            }

            if ($noResumeFlag) {
                // Update delivery status to error - missing resume
                update_delivery_status_missing_resume($this->applyId);

                QueueManager::pushTask((new SendApplicationEmailJob($application->id)), 'high');

                $this->delete();
            } elseif ($expiredJob) {
                // Send failed apply email to seeker
                (new MailService())->sendApplyFailureEmail($application);

                // Update delivery status to unsuccessful - job expired
                update_delivery_status_expired_job($this->applyId);
                $this->delete();
            } elseif ($this->attempts() >= 6 or $hardFail) {
                // Update delivery status to send by email in seeker_applies table
                update_delivery_status_email($this->applyId);

                QueueManager::pushTask((new SendApplicationEmailJob($application->id)), 'high');

                $haysApplyService->deleteDocuments($resumePath);
                $this->delete();
            } else {
                // Write failed apply details to seeker_applies table
                update_delivery_status_fail($this->applyId, $e->getMessage());

                $haysApplyService->deleteDocuments($resumePath);
                $this->release();
            }
        }
    }
}
